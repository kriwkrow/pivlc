# PiVLC

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/paypalme/kriwkrow)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Turn your Pi into a 4K media player. [Download latest!](https://gitlab.com/kriwkrow/pivlc/-/jobs/939339402/artifacts/raw/PiVLC.zip).

## How it Works?

Burn latest image (below), Copy your H.264 (1K) and H.265 (4K, Raspberry Pi 4 only) files into the `videos` directory of your USB flash drive. Raspberry Pi will boot up and play them one after another in a loop.

## Usage

### Step 01
Download the latest release (check above) PiVLC image file and burn it using [Raspberry Pi Imager](https://www.raspberrypi.org/software/).

### Step 02
Make sure display is connected to your Raspberry Pi. Insert the SD card into your Raspberry Pi and power it up. It will first expand the filesystem to fit the full capacity of your SD card. Demo videos should start playing on the screen.

### Step 03
Use a USB flash drive to play your videos. Copy videos you want to play in the `videos` directory of your USB flash drive and reboot your Pi. Anything with H.264 codec should be fine up to Raspberry Pi 3 Model B+. H.265 can be used with Raspberry Pi 4.

## Video Encoding
Anything with H.264 codec should be fine up to Raspberry Pi 3 Model B+. H.265 can be used with Raspberry Pi 4.

### 1K (Full HD) Encoding with H.264

```
ffmpeg -i video.mp4 -vf scale=1920:1080 -c:v libx264 -c:a aac -b:a 128k video-1k-h264.mp4
```

Scale is optional. It is used here to make sure you end up with actual 1080p resolution. The same goes for `-b:a`, can be any other bitrate.

### 4K Encoding with H.265

```
ffmpeg -i video.mp4 -vf scale=3840:2160 -c:v libx265 -crf 26 -preset fast -c:a aac -b:a 128k video-4k-h265.mp4
```

Scale is optional. It is used here to make sure you end up with actual 2160p resolution. The same goes for `-b:a`, can be any other bitrate.

## Support

Consider donating if you like the project. Please consider again if you are using it for a commercial purpose.

[![Donate Now!](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/paypalme/kriwkrow)

## Credits

Test video by [Seb Agora](https://pixabay.com/users/seb-agora-13890949/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=31851) from [Pixabay](https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=31851)
