#!/bin/bash -e

# Copy video files
mkdir ${ROOTFS_DIR}/home/pi/.pivlc
cp -r /builds/kriwkrow/pivlc/videos ${ROOTFS_DIR}/home/pi/.pivlc/
cp -r /builds/kriwkrow/pivlc/scripts ${ROOTFS_DIR}/home/pi/.pivlc/
echo "/home/pi/.pivlc/scripts/start.sh" >> ${ROOTFS_DIR}/home/pi/.bashrc

# Enter chroot on Raspberry Pi (act as if you were root on Pi)
on_chroot << EOF

# Change owner to pi
chown --recursive pi:pi /home/pi/.pivlc

# Set up autologin
systemctl set-default multi-user.target
ln -fs /lib/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@tty1.service
cat > /etc/systemd/system/getty@tty1.service.d/autologin.conf << EOL
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin pi --noclear %I \$TERM
EOL

# Fix usbmount
sed -i 's/PrivateMounts=yes/PrivateMounts=no/g' /lib/systemd/system/systemd-udevd.service

EOF

echo "PiVLC setup done!"
