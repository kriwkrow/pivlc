#!/bin/bash

# Stop here if SSH session
if [ -n "${SSH_CLIENT}" ] || [ -n "${SSH_TTY}" ]; then
  exit 0
fi

DEF_VIDS_DIR="/home/pi/.pivlc/videos"
USB_VIDS_DIR="/media/usb/videos"
PLAYLIST_DIR=${DEF_VIDS_DIR}
LOG_FILE="/home/pi/.pivlc/pivlc.log"

# Start PiVLC
echo "Starting PiVLC..."
# Wait for some time in case usbmount takes time
sleep 5
# Check if we have any videos in the USB
NUM_VIDS_MP4=$(ls ${USB_VIDS_DIR} | grep .mp4 | wc -l)
if (( ${NUM_VIDS_MP4} > 0 )); then
  echo "USB detected, contains ${NUM_VIDS_MP4} videos."
  PLAYLIST_DIR=${USB_VIDS_DIR}
fi;

/usr/bin/cvlc --intf http --http-port 4321 --http-password asd123 --loop --no-osd ${PLAYLIST_DIR} &> ${LOG_FILE} &
echo "PiVLC started!"
